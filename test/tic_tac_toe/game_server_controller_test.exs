defmodule TicTacToe.GameServerControllerTest do
  use ExUnit.Case

  # Note: Test are running in random

  test "check if DynamicSupervisor is available" do
    # Check if the TicTacToe.DynamicSupervisor is available
    assert Process.whereis(TicTacToe.DynamicSupervisor) |> is_pid == true
  end

  test "multiple moves on different processes" do
    # Create a game called `:game_server_1`
    TicTacToe.GameServerController.create_game(:game_server_1)

    # Check `:game_server_1` current player the initial player
    assert TicTacToe.GameServerController.get_current_player(:game_server_1) == :x

    # Create a game called `:game_server_2`
    TicTacToe.GameServerController.create_game(:game_server_2)

    # Check `:game_server_2` current player the initial player
    assert TicTacToe.GameServerController.get_current_player(:game_server_2) == :x

    # Make a move on `:game_server_1` for player x
    TicTacToe.GameServerController.make_move(:game_server_1, 1, 1)

    # Check `:game_server_1` current player that should be change from x to o
    assert TicTacToe.GameServerController.get_current_player(:game_server_1) == :o

    # Make a move on `:game_server_2` for player x
    TicTacToe.GameServerController.make_move(:game_server_2, 1, 1)

    # Make a move on `:game_server_2` for player o
    TicTacToe.GameServerController.make_move(:game_server_2, 2, 2)

    # After 2 moves that happened on `:game_server_2` the current player should be x
    assert TicTacToe.GameServerController.get_current_player(:game_server_2) == :x

    # Make the second move on `:game_server_1` for player o
    TicTacToe.GameServerController.make_move(:game_server_1, 1, 2)

    # Make an out of range move for player x on `:game_server_1`
    TicTacToe.GameServerController.make_move(:game_server_1, 5, 4)

    # Check the out of range message on `:game_server_1`
    assert TicTacToe.GameServerController.get_message(:game_server_1) ==
             "Make sure you pick be in range between 1 and 3"

    # After invalid move on `:game_server_1` the play should not change
    assert TicTacToe.GameServerController.get_current_player(:game_server_1) == :x
  end

  test "check score" do
    # Create a game called `:game_server_1`
    TicTacToe.GameServerController.create_game(:game_server_5)

    # player x
    TicTacToe.GameServerController.make_move(:game_server_5, 1, 1)

    # player o
    TicTacToe.GameServerController.make_move(:game_server_5, 2, 1)

    # player x
    TicTacToe.GameServerController.make_move(:game_server_5, 1, 2)

    # player o
    TicTacToe.GameServerController.make_move(:game_server_5, 2, 2)

    # player x
    %{player_o_wins: player_o_wins, player_x_wins: player_x_wins} =
      TicTacToe.GameServerController.make_move(:game_server_5, 1, 3)

    assert player_o_wins == 0
    assert player_x_wins == 1

    intial_state = %TicTacToe.Game{}
    game_state = TicTacToe.GameServerController.get_game_state(:game_server_5)
    assert game_state == intial_state

    # player x
    TicTacToe.GameServerController.make_move(:game_server_5, 2, 1)

    # player o
    TicTacToe.GameServerController.make_move(:game_server_5, 1, 1)

    # player x
    TicTacToe.GameServerController.make_move(:game_server_5, 2, 2)

    # player o
    TicTacToe.GameServerController.make_move(:game_server_5, 1, 2)

    # player x
    TicTacToe.GameServerController.make_move(:game_server_5, 3, 1)

    # player o
    %{player_o_wins: player_o_wins, player_x_wins: player_x_wins, game_state: game_state} =
      TicTacToe.GameServerController.make_move(:game_server_5, 1, 3)

    assert player_o_wins == 1
    assert player_x_wins == 1
    assert game_state == intial_state
  end
end
