defmodule TicTacToe.GameTest do
  use ExUnit.Case

  test "initial game state" do
    # Create a struct of TicTacToe.Game and save it in game
    game = %TicTacToe.Game{}

    # Check the initial state of the game
    assert game.board == [
             [:empty, :empty, :empty],
             [:empty, :empty, :empty],
             [:empty, :empty, :empty]
           ]

    assert game.current_player == :x
    assert game.game_over? == false
    assert game.message == ""
    assert game.moves == 9
    assert game.status == :ongoing
  end

  test "test make_move/3 with valid position" do
    game = %TicTacToe.Game{}

    assert game.current_player == :x
    game = TicTacToe.Game.make_move(game, 1, 2)
    assert game.current_player == :o

    assert game.board == [
             [:empty, :x, :empty],
             [:empty, :empty, :empty],
             [:empty, :empty, :empty]
           ]

    game = TicTacToe.Game.make_move(game, 2, 2)
    assert game.current_player == :x

    assert game.board == [
             [:empty, :x, :empty],
             [:empty, :o, :empty],
             [:empty, :empty, :empty]
           ]
  end

  test "test make_move/3 with invalid position" do
    game = %TicTacToe.Game{}

    assert game.current_player == :x

    # Make an valid move
    game = TicTacToe.Game.make_move(game, 1, 2)

    # Since it's a valid move the player should change
    assert game.current_player == :o

    # Check if the board state changed based on the previous move
    assert game.board == [
             [:empty, :x, :empty],
             [:empty, :empty, :empty],
             [:empty, :empty, :empty]
           ]

    # Make an invalid move since the cell is occupied
    game = TicTacToe.Game.make_move(game, 1, 2)

    # Recieve a message
    assert game.message == "Cell is already occupied!"

    # The player should not change since it's invalid move
    assert game.current_player == :o

    assert game.board == [
             [:empty, :x, :empty],
             [:empty, :empty, :empty],
             [:empty, :empty, :empty]
           ]

    game = TicTacToe.Game.make_move(game, 2, 2)
    assert game.message == "o on (2,2)"
    assert game.current_player == :x

    assert game.board == [
             [:empty, :x, :empty],
             [:empty, :o, :empty],
             [:empty, :empty, :empty]
           ]
  end

  test "test make_move/3 with out of range" do
    # Make an invalid move on col
    game =
      %TicTacToe.Game{}
      |> TicTacToe.Game.make_move(3, 5)

    assert game.moves == 9

    # The player should receive a message of his invalide move
    assert game.message === "Make sure you pick be in range between 1 and 3"

    # The player should not change since it's invalid move
    assert game.current_player === :x

    # Make an invalid move on row
    game = TicTacToe.Game.make_move(game, 0, 3)

    assert game.message === "Make sure you pick be in range between 1 and 3"

    # The player should not change since it's invalid move
    assert game.current_player === :x

    assert game.moves == 9

    # Make an valid move
    game = TicTacToe.Game.make_move(game, 1, 2)

    assert game.message === "x on (1,2)"

    # Since it's a valid move the player should change
    assert game.current_player === :o

    assert game.moves == 8

    # Make an invalid move on row and col
    game = TicTacToe.Game.make_move(game, 10, 8)
    assert game.message === "Make sure you pick be in range between 1 and 3"

    # The player should not change since it's invalid move
    assert game.current_player === :o
    assert game.moves == 8
  end

  test "Player x wins on the first column" do
    # Set of moves that fills player x the (1,1) and (1,2)
    game =
      %TicTacToe.Game{}
      # player x move
      |> TicTacToe.Game.make_move(1, 1)
      # player o move
      |> TicTacToe.Game.make_move(2, 1)
      # player x move
      |> TicTacToe.Game.make_move(1, 2)
      # player o move
      |> TicTacToe.Game.make_move(2, 2)

    # Check the last move there is no winner yet
    assert game.game_over? == false
    assert game.message == "o on (2,2)"
    assert game.moves == 5

    # Check the final move for player x as a winning move to fill the first column
    game = TicTacToe.Game.make_move(game, 1, 3)
    assert game.game_over? == true
    assert game.message == "Congrats x for winning"
    assert game.status == :x
  end

  test "Player o wins on diagonal" do
    # Set of moves that for player x to win on a diagonal
    game =
      %TicTacToe.Game{}
      |> TicTacToe.Game.make_move(1, 2)
      |> TicTacToe.Game.make_move(2, 2)
      |> TicTacToe.Game.make_move(1, 3)
      |> TicTacToe.Game.make_move(1, 1)
      |> TicTacToe.Game.make_move(2, 3)

    assert game.game_over? == false
    assert game.moves == 4

    assert ExUnit.CaptureIO.capture_io(fn ->
             TicTacToe.Game.print_board(game)
           end) == "o x x\n- o x\n- - -\n"

    game = TicTacToe.Game.make_move(game, 3, 3)
    assert game.game_over? == true
    assert game.message == "Congrats o for winning"
    assert game.status == :o

    assert ExUnit.CaptureIO.capture_io(fn ->
             TicTacToe.Game.print_board(game)
           end) == "o x x\n- o x\n- - o\n"
  end

  test "Draw" do
    game =
      %TicTacToe.Game{}
      |> TicTacToe.Game.make_move(2, 2)
      |> TicTacToe.Game.make_move(3, 3)
      |> TicTacToe.Game.make_move(3, 2)
      |> TicTacToe.Game.make_move(1, 2)
      |> TicTacToe.Game.make_move(2, 3)
      |> TicTacToe.Game.make_move(2, 1)
      |> TicTacToe.Game.make_move(3, 1)
      |> TicTacToe.Game.make_move(1, 3)
      |> TicTacToe.Game.make_move(1, 1)

    assert game.game_over? == true
    assert game.message == "Draw"
    assert game.moves == 0
    assert game.status == :draw
  end
end
