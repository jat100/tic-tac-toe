defmodule TicTacToe.GameControllerTest do
  use TicTacToeWeb.ConnCase, async: true
  alias TicTacToe.Helper

  test "create_game/2 creates a new game and returns success response", %{conn: conn} do
    conn1 = post(conn, ~p"/api/v1/create_game")
    assert conn1.status == 200

    %{id: id_1} = json_response(conn1)

    assert json_response(conn1) == %{
             message: "Game has been created successfully with the following id #{id_1}",
             id: id_1
           }

    conn2 = post(conn, ~p"/api/v1/create_game")

    %{id: id_2} = json_response(conn2)

    assert json_response(conn2) == %{
             message: "Game has been created successfully with the following id #{id_2}",
             id: id_2
           }
  end

  test "make_move/2 invalid params", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/make_move")

    assert json_response(conn) == %{
             error: "Make sure your body contains id, row and col"
           }

    assert conn.status == 400
  end

  test "make_move/2 invalid id", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/make_move", %{"id" => 100, "row" => 1, "col" => 2})

    assert json_response(conn) == %{
             error: "Id not found"
           }

    assert conn.status == 404
  end

  test "make_move/2 valid id", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/create_game")

    %{
      id: id
    } = json_response(conn)

    row = 1
    col = 2
    conn = post(conn, ~p"/api/v1/make_move", %{"id" => id, "row" => row, "col" => col})

    %{game_state: game_state} = json_response(conn)

    game = %TicTacToe.Game{} |> TicTacToe.Game.make_move(row, col)
    assert game |> Map.from_struct() == game_state |> Helper.conver_struct_to_atom()
    assert conn.status == 200
  end

  test "make_move/2 valid id multiple moves", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/create_game")

    %{
      id: id
    } = json_response(conn)

    row = 2
    col = 2
    conn = post(conn, ~p"/api/v1/make_move", %{"id" => id, "row" => row, "col" => col})

    game = %TicTacToe.Game{} |> TicTacToe.Game.make_move(row, col)

    row = 1
    col = 2
    conn = post(conn, ~p"/api/v1/make_move", %{"id" => id, "row" => row, "col" => col})

    game = TicTacToe.Game.make_move(game, row, col)

    row = 1
    col = 1
    conn = post(conn, ~p"/api/v1/make_move", %{"id" => id, "row" => row, "col" => col})

    game = TicTacToe.Game.make_move(game, row, col)

    %{game_state: game_state} = json_response(conn)

    assert game |> Map.from_struct() == game_state |> Helper.conver_struct_to_atom()
    assert conn.status == 200
  end

  test "current_player/2 with id", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/create_game")

    %{
      id: id
    } = json_response(conn)

    conn = get(conn, ~p"/api/v1/current_player?id=#{id}")
    assert conn.status == 200
    assert %{current_player: "x"} = json_response(conn)
  end

  test "current_player/2 without id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/current_player")
    assert conn.status == 400
    assert %{error: "Make sure your query string contain id"} == json_response(conn)
  end

  test "current_player/2 with invalid id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/current_player?id=102202")
    assert conn.status == 404

    assert %{
             error: "Id not found"
           } == json_response(conn)
  end

  test "get_score/2 with id", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/create_game")

    %{
      id: id
    } = json_response(conn)

    conn = get(conn, ~p"/api/v1/score?id=#{id}")
    assert conn.status == 200
    assert %{player_o: 0, player_x: 0} = json_response(conn)
  end

  test "get_score/2 without id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/score")
    assert conn.status == 400
    assert %{error: "Make sure your query string contain id"} == json_response(conn)
  end

  test "get_score/2 with invalid id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/score?id=102202")
    assert conn.status == 404

    assert %{
             error: "Id not found"
           } == json_response(conn)
  end

  test "get_state/2 with id", %{conn: conn} do
    conn = post(conn, ~p"/api/v1/create_game")

    %{
      id: id
    } = json_response(conn)

    conn = get(conn, ~p"/api/v1/state?id=#{id}")
    assert conn.status == 200

    game = %TicTacToe.Game{}
    %{game_state: game_state, player_o_wins: o_wins, player_x_wins: x_wins} = json_response(conn)
    assert game |> Map.from_struct() == game_state |> Helper.conver_struct_to_atom()
    assert o_wins == 0
    assert x_wins == 0
  end

  test "get_state/2 without id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/state")
    assert conn.status == 400
    assert %{error: "Make sure your query string contain id"} == json_response(conn)
  end

  test "get_state/2 with invalid id", %{conn: conn} do
    conn = get(conn, ~p"/api/v1/state?id=102202")
    assert conn.status == 404

    assert %{
             error: "Id not found"
           } == json_response(conn)
  end

  defp json_response(conn) do
    conn.resp_body
    |> Jason.decode!(keys: :atoms)
  end
end
