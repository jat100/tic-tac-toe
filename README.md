# TicTacToe

# Before running the project

- Please check that you have `elixir 1.14.1`
- To check please use `elixir --version`
- You should have the following output `Elixir 1.14.1 (compiled with Erlang/OTP 25)`

# Run the project

- Please clone the repo on your local machine
- Run `mix deps.get`
- Run `iex -S mix phx.server`

**Note**: Since the project is build using `--no-ecto` no need to run postgresql server on your local machine

# To play on the command interface

- Please make sure you follow the above steps to **run the project**
- `TicTacToe.play` this command will run the command interface
- On the command interface you will have the option to **play alone** or **against the computer**
- Incase you pick the computer the command interface will allow you to pick the level of the computer ability.
  - Level 1: Random pick from the possible moves that are left
  - Level 2: Smart moves based on [minimax algorithm](https://www.neverstopbuilding.com/blog/minimax)

# To play it on the browser

- Please run the server
- Go to http://localhost:4000/game
- To play it in multiplayer pick any id that is integer for example http://localhost:4000/game?id=20
- Open a new browser and put the same id to join the session

Note: There is an issue with boradcasting the opponite pick in real time. #5 was created

# Documentation

## Locally

- Use the following command `mix docs` to generate the docs on your machine
- To open the docs `open doc/index.html`

## Online

The documention of the project is hosted online on the following link https://tic-tac-toe-jat10.vercel.app/api-reference.html

# Tests

- Run `mix test` or `mix coveralls`

**Note**: Test stage is implemented so you can check the [pieplines here](https://gitlab.com/jat100/tic-tac-toe/-/pipelines)
