defmodule TicTacToe do
  @moduledoc """
  TicTacToe is a well known game reference https://en.wikipedia.org/wiki/Tic-tac-toe

  A project made with love by Jad Tarabay on June 18, 2023

  # Basic user stories
  The basic user stories for the minimal version of the game are:
  - As an API user I should be able to create a new tic tac toe game session
  - As an API user I should be able to complete a turn as the crosses (X) player
  - As an API user I should be able to complete a turn as the naughts (O) player
  - As an API user when I make a winning move, I should be informed and the game should be completed with a win status

  The basic game itself should be relatively easy to set up, there are several additional features that should be added, depending on your capabilities, preferred path. You must complete
  at least one additional feature on top of the basic requirement

  # Option 1: Player vs Computer
  Implement a version of the game where the computer will act as one of the players, and stop the player from making winning moves, while also trying to make a winning move.

  # Option 2: Multiplayer
  Allow two players to have some kind of session such that they could both use the API as separate actors and complete with each other

  # Option 3: Front end
  You can in theory develop everything so far driven by tests, and maybe and API or command line interface. For this option you can build a frontend for your game, anyway you like (live view would be preferred but not essential), and have the full stack operational
  """
  def play do
    IO.puts("Welcome to Tic Tac Toe")
    IO.puts("- If you want to play alone please pick 1")

    case IO.gets("- If you want to play vs computer please pick 2\n") do
      "1\n" ->
        TicTacToe.GamePlayTerminal.start()

      "2\n" ->
        TicTacToe.GamePlayComputer.start()
    end
  end
end
