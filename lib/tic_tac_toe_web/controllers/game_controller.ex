defmodule TicTacToeWeb.GameController do
  @moduledoc """
  TicTacToe.GameController is the API controller
  """
  use TicTacToeWeb, :controller

  alias TicTacToe.GameServerController
  alias TicTacToe.Helper

  @doc """
  API controller function to create the game using `TicTacToe.GameServerController`
  """
  def create_game(conn, _params) do
    id = GameServerController.get_id()

    {status, data} =
      case "game_#{id}" |> String.to_atom() |> GameServerController.create_game() do
        {:ok, _} ->
          {200,
           %{message: "Game has been created successfully with the following id #{id}", id: id}}

        {:error, _err} ->
          {400, %{message: "Something went wrong"}}
      end

    conn
    |> put_status(status)
    |> json(data)
  end

  @doc """
  API controller function to allow user to make a move based on a specific id
  """
  def make_move(conn, %{"id" => id, "row" => row, "col" => col}) do
    name = id |> get_name

    {status, data} =
      if name |> Helper.check_pid() do
        data = %{game_state: game_state} = GameServerController.make_move(name, row, col)
        data = %{data | game_state: game_state |> Map.from_struct()}
        {200, data}
      else
        {404, %{error: "Id not found"}}
      end

    conn
    |> put_status(status)
    |> json(data)
  end

  def make_move(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{error: "Make sure your body contains id, row and col"})
  end

  @doc """
  API controller function that returns the current player based on a specific id
  """
  def get_current_player(conn, %{"id" => id}) do
    name = id |> get_name

    {status, data} =
      if name |> Helper.check_pid() do
        player = GameServerController.get_current_player(name)
        {200, %{current_player: player}}
      else
        {404, %{error: "Id not found"}}
      end

    conn
    |> put_status(status)
    |> json(data)
  end

  def get_current_player(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{error: "Make sure your query string contain id"})
  end

  def get_score(conn, %{"id" => id}) do
    name = id |> get_name

    {status, data} =
      if name |> Helper.check_pid() do
        %{player_o_wins: player_o, player_x_wins: player_x} =
          GameServerController.get_details(name)

        {200, %{player_o: player_o, player_x: player_x}}
      else
        {404, %{error: "Id not found"}}
      end

    conn
    |> put_status(status)
    |> json(data)
  end

  @doc """
  API controller function that returns the score of the game based on a specific id
  """
  def get_score(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{error: "Make sure your query string contain id"})
  end

  def get_state(conn, %{"id" => id}) do
    name = id |> get_name

    {status, data} =
      if name |> Helper.check_pid() do
        data = %{game_state: game_state} = GameServerController.get_details(name)
        data = %{data | game_state: game_state |> Map.from_struct()}
        {200, data}
      else
        {404, %{error: "Id not found"}}
      end

    conn
    |> put_status(status)
    |> json(data)
  end

  def get_state(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{error: "Make sure your query string contain id"})
  end

  defp get_name(id) do
    "game_#{id}" |> String.to_atom()
  end
end
