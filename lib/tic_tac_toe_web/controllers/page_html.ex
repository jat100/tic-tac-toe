defmodule TicTacToeWeb.PageHTML do
  @moduledoc false
  use TicTacToeWeb, :html

  embed_templates "page_html/*"
end
