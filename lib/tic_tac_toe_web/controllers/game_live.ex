defmodule TicTacToeWeb.GameLive do
  use Phoenix.LiveView

  alias TicTacToe.GameServerController
  alias TicTacToe.Helper

  def render(assigns) do
    ~L"""
    <div class="container mx-auto">
      <h1 class="mt-2 mb-10">Tic Tac Toe</h1>
      <div class="flex">
        <div class="mr-2">Score:</div>
        <div class="flex">
        <div>Player X: <%= @player_x %></div>
        <div class="mx-2">-</div>
        <div>Player O: <%= @player_o %></div>
        </div>
      </div>
      <p class="mb-2">Current player: <%= @current_player %></p>
        <%= for row <- Enum.with_index(@board) do %>
        <div class="row flex mb-3">
          <%= for cell <- row |> elem(0) |> Enum.with_index() do %>
            <div class="cell mr-3 w-[50px] h-[50px] bg-gray-500 text-white hover:cursor-pointer flex justify-center items-center text-[40px]" phx-click="cell_clicked" phx-value-row="<%= row |> elem(1) %>" phx-value-col="<%= cell |> elem(1) %>" phx-value-id="<%= @id %>">
              <%= if cell |> elem(0) == "empty", do: "", else: cell |> elem(0) %>
            </div>
          <% end %>
        </div>
      <% end %>
    </div>
    """
  end

  def init(id, socket, session) do
    name = get_name(id)

    %{game_state: game, player_o_wins: player_o, player_x_wins: player_x} =
      GameServerController.get_details(name)

    game_string = game |> Helper.conver_struct_to_string()

    {:ok,
     assign(socket,
       board: game_string.board,
       current_player: game_string.current_player,
       game: game,
       player_x: player_x,
       player_o: player_o,
       id: id,
       session: session
     )}
  end

  def handle_params(_params, _url, socket) do
    {:noreply, assign(socket, session: fetch_session(socket))}
  end

  defp fetch_session(socket) do
    case socket.assigns.session do
      nil -> :ignore
      _ -> socket.assigns.session
    end
  end

  def mount(%{"id" => id}, session, socket) do
    name = id |> get_name()

    if name |> Helper.check_pid() do
      id
      |> init(socket, session)
    else
      push_redirect(socket, to: "/game")
      id = GameServerController.get_id()

      id |> get_name() |> GameServerController.create_game()
      init(id, socket, session)
    end
  end

  def mount(_params, session, socket) do
    id = GameServerController.get_id()

    id |> get_name() |> GameServerController.create_game()

    init(id, socket, session)
  end

  def handle_event("cell_clicked", %{"row" => row, "col" => col, "id" => id}, socket) do
    row_int = row |> String.to_integer()
    col_int = col |> String.to_integer()
    name = get_name(id)

    %{game_state: game, player_o_wins: player_o, player_x_wins: player_x} =
      GameServerController.make_move(name, row_int + 1, col_int + 1)

    game_string = game |> Helper.conver_struct_to_string()

    {:noreply,
     assign(socket,
       board: game_string.board,
       current_player: game_string.current_player,
       game: game,
       player_x: player_x,
       player_o: player_o,
       id: id
     )}
  end

  defp get_name(id) do
    "game_#{id}" |> String.to_atom()
  end
end
