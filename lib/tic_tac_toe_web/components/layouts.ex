defmodule TicTacToeWeb.Layouts do
  @moduledoc false
  use TicTacToeWeb, :html

  embed_templates "layouts/*"
end
