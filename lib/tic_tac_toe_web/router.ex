defmodule TicTacToeWeb.Router do
  @moduledoc false
  use TicTacToeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TicTacToeWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TicTacToeWeb do
    pipe_through :browser

    get "/", PageController, :home
    live "/game", GameLive
  end

  scope "/api/v1", TicTacToeWeb do
    pipe_through :api

    post("/create_game", GameController, :create_game)
    post("/make_move", GameController, :make_move)
    get("/current_player", GameController, :get_current_player)
    get("/score", GameController, :get_score)
    get("/state", GameController, :get_state)
  end

  # Other scopes may use custom stacks.
  # scope "/api", TicTacToeWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard in development
  if Application.compile_env(:tic_tac_toe, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: TicTacToeWeb.Telemetry
    end
  end
end
