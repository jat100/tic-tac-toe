defmodule TicTacToe.GamePlayTerminal do
  @moduledoc """
  TicTacToe.GamePlayTerminal is a module that allows users to play on the Command Interface (iex)

  ## Struct
      iex> %TicTacToe.GamePlayTerminal{}
      %TicTacToe.GamePlayTerminal{
        match: %TicTacToe.Game{
          board: [
            [:empty, :empty, :empty],
            [:empty, :empty, :empty],
            [:empty, :empty, :empty]
          ],
          current_player: :x,
          game_over?: false,
          message: ""
        },
        player_x_wins: 0,
        player_o_wins: 0
      }
  """
  defstruct match: %TicTacToe.Game{}, player_x_wins: 0, player_o_wins: 0

  @doc """
  To start the game use `TicTacToe.GamePlayTerminal.start`
  """
  def start() do
    game_play = %TicTacToe.GamePlayTerminal{}
    IO.puts("TicTacToe game just started")
    IO.puts("- When you want to enter the value make sure to seperate it with ,")
    IO.puts("- To end the game please type end")

    loop(game_play)
  end

  defp start(game) do
    %{game | match: %TicTacToe.Game{}}
    |> stats
    |> loop
  end

  defp stats(game) do
    IO.puts("Player x win(s): #{game.player_x_wins}")
    IO.puts("Player o win(s): #{game.player_o_wins}")
    game
  end

  defp continue_play(game) do
    case IO.gets("Do you want to continue playing (y/n)?\n") do
      "y\n" ->
        start(game)

      "n\n" ->
        IO.puts("Here is the final result")
        stats(game)
        IO.puts("Thanks for playing")
    end
  end

  defp loop(game) do
    game_state = game.match
    IO.puts("Player #{game_state.current_player}, it's your turn.")
    TicTacToe.Game.print_board(game_state)

    case IO.gets("Enter your move (row,col):\n") do
      "stats\n" ->
        stats(game)
        loop(game)

      "end\n" ->
        IO.puts("Game aborted.")
        TicTacToe.play()

      input ->
        [row, col] =
          String.trim(input)
          |> String.split(",")
          |> Enum.map(&String.to_integer/1)

        new_game = %{game | match: TicTacToe.Game.make_move(game_state, row, col)}

        if new_game.match.game_over? do
          IO.puts("#{new_game.match.message}")

          if new_game.match.current_player == :o do
            %{new_game | player_x_wins: new_game.player_x_wins + 1}
          else
            %{new_game | player_o_wins: new_game.player_o_wins + 1}
          end
          |> continue_play
        else
          loop(new_game)
        end
    end
  end
end
