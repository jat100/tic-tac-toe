defmodule TicTacToe.GameServerController do
  @moduledoc """
  GameServer Controller is used to manage the communication with different GenServers that is supervised by TicTacToe.DynamicSupervisor
  """

  @doc """
  To create a new Game with a specific name you can call TicTacToe.GameServerController.create_game(any_name)

  ## Example
      iex> TicTacToe.GameServerController.create_game(:game_1)
      {:ok, pid()}
  """
  def create_game(name) do
    DynamicSupervisor.start_child(TicTacToe.DynamicSupervisor, {TicTacToe.GameServer, name})
  end

  @doc """
  To get the state of the created game at any stage of the game

  ## Example
      iex> TicTacToe.GameServerController.get_details(:game_1)
      %{
        game_state: %TicTacToe.Game{
          board: [
            [:empty, :empty, :empty],
            [:empty, :empty, :empty],
            [:empty, :empty, :empty]
          ],
          current_player: :x,
          game_over?: false,
          message: ""
        },
        player_o_wins: 0,
        player_x_wins: 0
      }
  """
  def get_details(name) do
    GenServer.call(name, :get_state)
  end

  @doc """
  To get the state of the created game at any stage of the game

  ## Example
      iex> TicTacToe.GameServerController.get_game_state(:game_1)
      %TicTacToe.Game{
        board: [
          [:empty, :empty, :empty],
          [:empty, :empty, :empty],
          [:empty, :empty, :empty]
        ],
        current_player: :x,
        game_over?: false,
        message: ""
      }
  """
  def get_game_state(name) do
    GenServer.call(name, :get_game_state)
  end

  @doc """
  To get the board based on the Genserver state

  ## Example
      iex> TicTacToe.GameServerController.get_board(:game_1)
      [[:empty, :empty, :empty], [:empty, :empty, :empty], [:empty, :empty, :empty]]
  """
  def get_board(name) do
    GenServer.call(name, :get_game_state) |> Map.get(:board)
  end

  @doc """
  To get the current player based on the Genserver state

  ## Example
      iex> TicTacToe.GameServerController.get_current_player(:game_1)
      :x
  """
  def get_current_player(name) do
    GenServer.call(name, :get_game_state) |> Map.get(:current_player)
  end

  @doc """
  To check if the game is over

  ## Example
      iex> TicTacToe.GameServerController.get_game_over(:game_1)
      false
  """
  def get_game_over(name) do
    GenServer.call(name, :get_game_state) |> Map.get(:game_over?)
  end

  @doc """
  To get the message from %TicTacToe.Game{} state that is saved in the GenServer
  """
  def get_message(name) do
    GenServer.call(name, :get_game_state) |> Map.get(:message)
  end

  @doc """
  Allow player to make a move on the board
  """
  def make_move(name, row, col) do
    GenServer.call(name, {:make_move, row, col})
  end

  def get_id() do
    %{active: active} = DynamicSupervisor.count_children(TicTacToe.DynamicSupervisor)
    active + 1
  end
end
