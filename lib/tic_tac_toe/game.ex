defmodule TicTacToe.Game do
  @moduledoc """
  TicTacToe.Game contains the logic of this game

  ## Initial State of the game
      iex> %TicTacToe.Game{}
      %TicTacToe.Game{
        board: [
          [:empty, :empty, :empty],
          [:empty, :empty, :empty],
          [:empty, :empty, :empty]
        ],
        current_player: :x,
        game_over?: false,
        message: ""
      }
  """
  @initial_board Enum.map(1..3, fn _ -> List.duplicate(:empty, 3) end)

  defstruct board: @initial_board,
            current_player: :x,
            game_over?: false,
            status: :ongoing,
            moves: 9,
            message: ""

  def make_move(game_state, row, col, player) when row > 0 and row < 4 and col > 0 and col < 4 do
    case get_cell(game_state.board, row, col) do
      :empty ->
        player_move(game_state, row, col, player)

      _ ->
        Map.put(game_state, :message, "Cell is already occupied!")
    end
  end

  @doc """
  Allow the current player to pick a move specifying the row and col of his play

  ## Example
      iex> game = %TicTacToe.Game{}
      iex> game = TicTacToe.Game.make_move(game,2,2)
      %TicTacToe.Game{
        board: [
          [:empty, :empty, :empty],
          [:empty, :x, :empty],
          [:empty, :empty, :empty]
        ],
        current_player: :o,
        game_over?: false,
        message: "x on (2,2)"
      }
  """
  def make_move(game_state, row, col) when row > 0 and row < 4 and col > 0 and col < 4 do
    case get_cell(game_state.board, row, col) do
      :empty ->
        player = game_state.current_player
        player_move(game_state, row, col, player)

      _ ->
        Map.put(game_state, :message, "Cell is already occupied!")
    end
  end

  def make_move(game_state, _row, _col) do
    Map.put(game_state, :message, "Make sure you pick be in range between 1 and 3")
  end

  defp player_move(game_state, row, col, player) do
    new_board = set_cell(game_state.board, row, col, player)

    new_game_state = %{
      game_state
      | board: new_board,
        current_player: next_player(player),
        message: "#{player} on (#{row},#{col})",
        moves: game_state.moves - 1
    }

    check_game_over(new_game_state)
  end

  defp get_cell(board, row, col), do: Enum.at(Enum.at(board, row - 1), col - 1)

  defp set_cell(board, row, col, value) do
    Enum.with_index(board)
    |> Enum.reduce([], fn {row_list, i}, acc ->
      if i == row - 1 do
        new_list =
          Enum.with_index(row_list)
          |> Enum.reduce([], fn {cell, j}, inner_acc ->
            if j == col - 1 do
              [value | inner_acc]
            else
              [cell | inner_acc]
            end
          end)
          |> Enum.reverse()

        [new_list | acc]
      else
        [row_list | acc]
      end
    end)
    |> Enum.reverse()
  end

  def next_player(:x), do: :o
  def next_player(:o), do: :x

  def check_game_over(game_state) do
    win_conditions = [
      # Rows
      [{1, 1}, {1, 2}, {1, 3}],
      [{2, 1}, {2, 2}, {2, 3}],
      [{3, 1}, {3, 2}, {3, 3}],
      # Columns
      [{1, 1}, {2, 1}, {3, 1}],
      [{1, 2}, {2, 2}, {3, 2}],
      [{1, 3}, {2, 3}, {3, 3}],
      # Diagonals
      [{1, 1}, {2, 2}, {3, 3}],
      [{1, 3}, {2, 2}, {3, 1}]
    ]

    win =
      win_conditions
      |> Enum.any?(fn positions ->
        positions
        |> Enum.map(fn {row, col} -> get_cell(game_state.board, row, col) end)
        |> same_elements?()
      end)

    if win == true do
      %{
        game_state
        | game_over?: true,
          message: "Congrats #{next_player(game_state.current_player)} for winning",
          status: next_player(game_state.current_player)
      }
    else
      if game_state.moves === 0 do
        %{
          game_state
          | game_over?: true,
            message: "Draw",
            status: :draw
        }
      else
        game_state
      end
    end
  end

  defp same_elements?([h | t]) when is_list(t) do
    Enum.all?(t, fn elem -> elem == h and elem != :empty end)
  end

  defp same_elements?([_]), do: false
  defp same_elements?(_), do: false

  def print_board(game_state) do
    game_state.board
    |> Enum.map(
      &Enum.map(&1, fn cell ->
        if cell == :empty, do: "-", else: to_string(cell)
      end)
    )
    |> Enum.map(&Enum.join(&1, " "))
    |> Enum.each(&IO.puts/1)
  end
end
