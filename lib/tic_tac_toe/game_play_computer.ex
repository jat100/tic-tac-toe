defmodule TicTacToe.GamePlayComputer do
  defstruct match: %TicTacToe.Game{},
            player: 0,
            computer: 0,
            player_pick: nil,
            available_moves: 9,
            level: 1

  def start() do
    IO.puts("TicTacToe game just started")
    IO.puts("- When you want to enter the value make sure to seperate it with ,")
    IO.puts("- To end the game please type end")

    %TicTacToe.GamePlayComputer{}
    |> level_pick()
    |> user_pick()
  end

  defp level_pick(game_play) do
    IO.puts("Computer can be dump (level 1) or smart (level 2)")

    case IO.gets("Please pick the level (1/2)\n") do
      "1\n" ->
        %{game_play | level: 1}

      "2\n" ->
        %{game_play | level: 2}

      "end\n" ->
        abort()
    end
  end

  defp user_pick(game_play) do
    stats(game_play)

    case IO.gets("Please pick where you want X or O (x/o)\n") do
      "x\n" ->
        %{game_play | player_pick: :x}
        |> start_game

      "o\n" ->
        %{game_play | player_pick: :o}
        |> start_game

      "end\n" ->
        abort()

      _ ->
        user_pick(game_play)
    end
  end

  defp start_game(game) do
    game_state = game.match
    IO.puts("Player #{game_state.current_player}, it's your turn.")
    TicTacToe.Game.print_board(game_state)

    if(game.available_moves == 9 and game.player_pick == :o) do
      computer_play(game)
    else
      player_play(game)
    end
  end

  def computer_play(game) do
    game_state = game.match
    IO.puts("Computer ")

    {row, col} =
      case game.level do
        1 ->
          game.match.board |> possible_moves |> Enum.random()

        2 ->
          game |> smart_move() |> elem(1)
      end

    %{game | match: TicTacToe.Game.make_move(game_state, row, col)}
    |> check_game
  end

  defp stats(game) do
    IO.puts("Player win(s): #{game.player}")
    IO.puts("Computer win(s): #{game.computer}")
    game
  end

  def player_play(game) do
    game_state = game.match

    case IO.gets("Enter your move (row,col):\n") do
      "stats\n" ->
        stats(game)
        player_play(game)

      "end\n" ->
        IO.puts("Game aborted.")

      input ->
        [row, col] =
          String.trim(input)
          |> String.split(",")
          |> Enum.map(&String.to_integer/1)

        case TicTacToe.Game.make_move(game_state, row, col) do
          %{message: "Make sure you pick be in range between 1 and 3"} ->
            player_play(game_state)

          match ->
            %{game | match: match}
            |> check_game
        end
    end
  end

  def check_game(game) do
    game_state = game.match

    if game.match.game_over? do
      IO.puts("#{game.match.message}")

      if game.match.current_player == :o and game.player_pick == :o do
        %{game | computer: game.computer + 1}
      else
        %{game | player: game.player + 1}
      end
      |> Map.put(:match, %TicTacToe.Game{})
      |> Map.put(:available_moves, 9)
      |> user_pick
    else
      TicTacToe.Game.print_board(game_state)
      new_state = %{game | available_moves: game.available_moves - 1}

      if game.match.current_player == game.player_pick do
        player_play(new_state)
      else
        computer_play(new_state)
      end
    end
  end

  def possible_moves(board) do
    Enum.flat_map(0..2, fn row ->
      Enum.map(0..2, fn col ->
        case get_cell(board, row, col) do
          :empty -> {row + 1, col + 1}
          _ -> nil
        end
      end)
    end)
    |> Enum.reject(&is_nil/1)
  end

  def smart_move(data, depth \\ 0) do
    best_score = -9999
    best_move = nil
    computer_pick = TicTacToe.Game.next_player(data.player_pick)
    player_pick = data.player_pick
    possible = possible_moves(data.match.board)

    Enum.reduce(possible, {-9999, possible |> Enum.at(0)}, fn move = {row, col}, {score, best} ->
      updated_board = TicTacToe.Game.make_move(data.match, row, col, computer_pick)

      new_score =
        -minimax(
          %{data | match: updated_board},
          player_pick,
          depth + 1,
          data.match.moves
        )

      if new_score > score do
        {new_score, {row, col}}
      else
        {score, best}
      end
    end)
  end

  def minimax(game, player, depth, max_depth) when depth <= max_depth do
    case TicTacToe.Game.check_game_over(game.match) do
      %TicTacToe.Game{status: :o} when depth > 0 ->
        if game.player_pick == :o do
          depth - 10
        else
          10 - depth
        end

      %TicTacToe.Game{status: :x} when depth > 0 ->
        if game.player_pick == :x do
          depth - 10
        else
          10 - depth
        end

      %TicTacToe.Game{status: :draw} ->
        0

      %TicTacToe.Game{status: :ongoing} ->
        if player == :x do
          Enum.reduce(possible_moves(game.match.board), 0, fn {row, col}, best_score ->
            updated_board = TicTacToe.Game.make_move(game.match, row, col, player)

            score =
              -minimax(
                %{game | match: updated_board},
                :o,
                depth + 1,
                max_depth
              )

            best_score + score
          end)
        else
          Enum.reduce(possible_moves(game.match.board), 0, fn {row, col}, best_score ->
            updated_board = TicTacToe.Game.make_move(game.match, row, col, player)

            score =
              -minimax(
                %{game | match: updated_board},
                :x,
                depth + 1,
                max_depth
              )

            best_score + score
          end)
        end
    end
  end

  def minimax(game, player, depth, max_depth) do
    0
  end

  defp get_cell(board, row, col) do
    List.flatten(board) |> Enum.at(row * 3 + col)
  end

  def abort() do
    IO.puts("Game aborted.")
    TicTacToe.play()
  end
end
