defmodule TicTacToe.GameServer do
  @moduledoc """
  TicTacToe.GameServer is the GenServer module
  """
  use GenServer

  def start_link(name) do
    GenServer.start_link(__MODULE__, %{}, name: name)
  end

  # GenServer callbacks
  def init(_) do
    {:ok, %{game_state: %TicTacToe.Game{}, player_x_wins: 0, player_o_wins: 0}}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:get_game_state, _from, state) do
    {:reply, state.game_state, state}
  end

  def handle_call({:make_move, row, col}, _from, state) do
    game_state = TicTacToe.Game.make_move(state.game_state, row, col)

    state =
      if game_state.game_over? do
        case game_state.message do
          "Congrats x for winning" ->
            %{
              state
              | game_state: %TicTacToe.Game{},
                player_x_wins: Map.get(state, :player_x_wins) + 1
            }

          "Congrats o for winning" ->
            %{
              state
              | game_state: %TicTacToe.Game{},
                player_o_wins: Map.get(state, :player_o_wins) + 1
            }

          _ ->
            %{state | game_state: game_state}
        end
      else
        %{state | game_state: game_state}
      end

    {:reply, state, state}
  end
end
