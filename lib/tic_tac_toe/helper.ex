defmodule TicTacToe.Helper do
  def conver_struct_to_atom(
        param = %{board: board, current_player: current_player, status: status}
      ) do
    board_atom = board |> Enum.map(&(&1 |> Enum.map(fn val -> val |> String.to_atom() end)))
    current_player_atom = current_player |> String.to_atom()

    %{
      param
      | board: board_atom,
        current_player: current_player_atom,
        status: status |> String.to_atom()
    }
  end

  def conver_struct_to_string(
        param = %{board: board, current_player: current_player, status: status}
      ) do
    board_atom = board |> Enum.map(&(&1 |> Enum.map(fn val -> val |> Atom.to_string() end)))
    current_player_atom = current_player |> Atom.to_string()

    %{
      param
      | board: board_atom,
        current_player: current_player_atom,
        status: status |> Atom.to_string()
    }
  end

  def check_pid(name) do
    name |> Process.whereis() |> is_pid
  end
end
